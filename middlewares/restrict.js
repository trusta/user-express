'use strict';

/**
 * Module dependencies
 */
var userServiceFunc = require('../services/userService');
var tokenServiceFunc = require('../services/tokenService');
var responseBuilder = require('../services/responseBuilder');

module.exports = function (config, logger) {
    var tokenService = tokenServiceFunc(logger);
    var userService = userServiceFunc(config, logger);

    logger.info('Defining restrict');

    function restrictUser(req, res, next) {
        var token = req.session.token;
        if (!token) {
            return responseBuilder.error(res);
        }

        var userId = tokenService.getUserId(token);
        if (!userId) {
            return responseBuilder.error(res);
        }

        userService.findUserById(userId, function (err, user) {
            if (err) {
                return responseBuilder.error(res);
            }

            if (!user) {
                return responseBuilder.error(res);
            }

            if (!tokenService.checkLoginToken(token, user)) {
                return responseBuilder.error(res);
            }
            req.user = user;
            return next();
        });
    }

    function restrictAdmin(req, res, next) {
        var token = req.session.token;
        if (!token) {
            return responseBuilder.error(res);
        }

        var userId = tokenService.getUserId(token);
        if (!userId) {
            return responseBuilder.error(res);
        }

        userService.findUserById(userId, function (err, user) {
            if (err) {
                return responseBuilder.error(res);
            }

            if (!user) {
                return responseBuilder.error(res);
            }

            if (!tokenService.checkLoginToken(token, user)) {
                return responseBuilder.error(res);
            }

            if (!user.admin) {
                return responseBuilder.error(res);
            }

            req.user = user;
            return next();
        });
    }

    return {
        user: restrictUser,
        admin: restrictAdmin
    };
};
