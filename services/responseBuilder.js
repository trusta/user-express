'use strict';

var responseBuilder = module.exports;

responseBuilder.error = function(res, error) {
    res.status(403);
    if (error) {
        res.json({
            error: error || 'An internal error occurred.'
        });
    }
    res.end();
};

responseBuilder.success = function(res, result) {
    if (result) {
        res.json({
            result: result
        });
    }
    res.end();
};
