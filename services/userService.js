'use strict';

var mongoDbRepository = require('../repository/mongoDbRepository');
var pwd = require('couch-pwd');

module.exports = function (config, logger) {
    var repository = mongoDbRepository(config, logger);

    function register(name, email, password, cb) {

        var user = {
            name: name,
            email: email
        };
        user.signupTimestamp = new Date();
        user.failedLoginAttempts = 0;
        user.accountLocked = false;

        repository.findUserByName(user.name, function (err, u) {
            if (err) {
                return cb(err);
            } else if (u) {
                return cb('A user with the name "' + user.name + '" already exists.');
            }

            repository.findUserByEmail(user.email, function (err, u) {
                if (err) {
                    return cb(err);
                } else if (u) {
                    return cb('A user with the email "' + user.email + '" already exists.');
                }

                // create salt and hash
                pwd.hash(password, function (err, salt, hash) {
                    if (err) {
                        return cb(err);
                    }
                    user.salt = salt;
                    user.derived_key = hash;

                    if (config.emailSignup) {
                        user.emailVerificationTimestamp = new Date();
                        user.emailVerified = false;
                    }

                    repository.createUser(user, function (err, user) {
                        if (err) {
                            return cb(err);
                        }
                        user._id = user._id.toString();
                        return cb(false, user);
                    });
                });
            });
        });
    }

    function verifyEmail(user, cb) {
        user.emailVerified = true;

        repository.updateUser(user, function (err) {
            cb(err);
        });
    }


    function resendConfirmationEmail(email, cb) {
        if (!config.emailSignup) {
            return cb('Confirmation by email is disabled');
        }

        repository.findUserByEmail(email, function (err, user) {
            if (err) {
                return cb(err);
            } else if (!user) {
                return cb('No user with this email found');
            } else if (user.emailVerified) {
                return cb('Email already verified');
            }
            user.emailVerificationTimestamp = new Date();

            repository.updateUser(user, function (err) {
                return cb(err, user);
            });
        });

    }

    function login(loginObj, cb) {

        function callback(err, user) {
            if (err) {
                logger.error('Error when try to login', err);
                return cb(err);
            } else if (!user) {
                return cb('Invalid login/password');
            } else if (user.accountLocked) {
                return cb('User locked');
            } else if (!user.emailVerified) {
                return cb('User\'s email not verified');
            } else if (user.forgotPasswordVerification) {
                return cb('User ask for reset password');
            }

            checkPassword(user, loginObj.password, function (err, invalidCredentials) {
                    if (err) {
                        return cb(err);
                    }
                    if (invalidCredentials) {
                        logger.info(invalidCredentials);

                        user.failedLoginAttempts++;

                        if (user.failedLoginAttempts >= config.failedLoginAttempts) {
                            user.accountLocked = true;
                        }

                        // Save user
                        return repository.updateUser(user, function (err) {
                            if (err) {
                                return cb(err);
                            }
                            return cb(invalidCredentials);
                        });

                    }

                    if (user.currentLoginTime) {
                        user.previousLoginTime = user.currentLoginTime;
                    }
                    user.currentLoginTime = new Date();
                    user.failedLoginAttempts = 0;

                    repository.updateUser(user, function (err) {
                        if (err) {
                            return cb(err);
                        }
                        return cb(false, user);
                    });
                }
            )
            ;
        }

        if (loginObj.name) {
            repository.findUserByName(loginObj.name, callback);
        } else if (loginObj.email) {
            repository.findUserByEmail(loginObj.email, callback);
        }
    }

    function logout(user, callback) {
        user.previousLoginTime = user.currentLoginTime;
        user.currentLoginTime = null;

        repository.updateUser(user, function (err) {
            if (err) {
                return callback(err);
            }
            return callback(false);
        });
    }

    function findUserById(userId, callback) {
        repository.findUserById(userId, function (err, user) {
            if (err) {
                return callback(err);
            } else if (!user) {
                return callback('User not found');
            }
            return callback(false, user);
        });
    }

    function checkPassword(user, password, cb) {
        // compare credentials with data in db
        pwd.hash(password, user.salt, function (err, hash) {
            if (err) {
                return cb(err);
            }

            if (hash !== user.derived_key) {
                return cb(false, 'Invalid login/password');
            }

            return cb(false);
        });
    }

    function updateUser(userId, userObj, cb) {

        findUserById(userId, function (err, user) {
            if (err) {
                logger.error('Error when trying to find user by id :', userId, err);
                return cb(err);
            }
            logger.debug('user found');

            checkPassword(user, userObj.password, function (err, invalidCredentials) {
                if (err) {
                    return cb(err);
                }
                if (invalidCredentials) {
                    return cb(invalidCredentials);
                }

                repository.findOtherUserByName(userId, userObj.name, function (err, u) {
                    if (err) {
                        return cb(err);
                    } else if (u) {
                        return cb('A user with the name "' + u.name + '" already exists.');
                    }
                    logger.debug('no user found with this name');

                    repository.findOtherUserByEmail(userId, userObj.email, function (err, u) {
                        if (err) {
                            return cb(err);
                        } else if (u) {
                            return cb('A user with the email "' + u.email + '" already exists.');
                        }
                        logger.debug('no user found with this email');

                        logger.debug('update user');
                        // Update user
                        user.name = userObj.name;
                        user.email = userObj.email;


                        repository.updateUser(user, function (err) {
                            if (err) {
                                return cb(err);
                            }
                            return cb(false, user);
                        });
                    });
                });
            });
        });

    }

    function deleteUser(userId, callback) {
        repository.deleteUser(userId, callback);
    }

    function resetPassword(user, newPassword, callback) {
        // create salt and hash
        pwd.hash(newPassword, function (err, salt, hash) {
            if (err) {
                return callback(err);
            }
            user.salt = salt;
            user.derived_key = hash;

            if (user.forgotPasswordVerification) {
                delete user.forgotPasswordVerification;

                var attributesToRemove = {
                    forgotPasswordVerification: 1
                };
                repository.updateUserAndRemoveAttributes(user, attributesToRemove, callback);
            } else {
                repository.updateUser(user, callback);
            }
        });
    }

    function changePassword(userId, oldPassword, newPassword, callback) {
        findUserById(userId, function (err, user) {
            if (err) {
                logger.error('Error when trying to find user by id :', userId, err);
                return callback(err);
            }

            checkPassword(user, oldPassword, function (err, invalidCredentials) {
                if (err) {
                    return callback(err);
                }
                if (invalidCredentials) {
                    return callback(invalidCredentials);
                }

                resetPassword(user, newPassword, callback);
            });
        });
    }

    function forgotPassword(email, callback) {
        repository.findUserByEmail(email, function (err, user) {
            if (err) {
                return callback(err);
            } else if (!user) {
                return callback('No user with the email ' + email + ' found');
            } else if (!user.emailVerified) {
                return callback('Account not activated');
            } else if (user.accountLocked) {
                return callback('Account locked');
            }

            user.forgotPasswordVerification = new Date();

            if (user.currentLoginTime) {
                user.previousLoginTime = user.currentLoginTime;
                user.currentLoginTime = null;
            }

            repository.updateUser(user, function (err) {
                if (err) {
                    return callback(err);
                }
                return callback(err, user);
            });
        });
    }


    return {
        register: register,
        verifyEmail: verifyEmail,
        resendConfirmationEmail: resendConfirmationEmail,
        login: login,
        logout: logout,
        findUserById: findUserById,
        checkPassword: checkPassword,
        updateUser: updateUser,
        deleteUser: deleteUser,
        changePassword: changePassword,
        forgotPassword: forgotPassword,
        resetPassword: resetPassword
    };
};
