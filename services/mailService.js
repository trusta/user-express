'use strict';

var nodemailer = require('nodemailer');
var ejs = require('ejs');

module.exports = function (config, logger) {
    if (config.mailer === undefined) {
        logger.error('There is no config to send mails');
        return;
    }

    var transporter = nodemailer.createTransport(config.mailer.transporterConfig);

    function send(template, email, username, link, callback) {
        // Check if the mailer is enabled
        if (!config.mailer) {
            callback();
            return;
        }

        logger.info('Sending', template, 'mail to', email);


        // Values to insert in the mail body
        var variables = {
            appname: config.appname,
            link: link,
            username: username
        };

        // set variables for the global template mail
        var locals = {
            title: config[template].title,
            content: ejs.render(config[template].text, variables)
        };

        // options of the mail
        var options = {
            from: config.emailFrom,
            to: email,
            subject: ejs.render(config[template].subject, variables),
            html: ejs.render(config.emailBase, locals)
        };
        logger.debug('email :', options);

        // Send the mail
        transporter.sendMail(options, function (err) {
            if (err) {
                logger.error('Error when sending', template, 'mail to', email, ':', err);
                transporter.close();
                return callback(err);
            }
            logger.info('Email send with success');
            transporter.close(); // shut down the connection pool, no more messages
            logger.info('transporter closed');
            callback();
        });

    }

    function signup(email, username, token, callback) {
        var link = '<a href="' + config.url + '/register/' + token + '">' + config.emailSignup.linkText + '</a>';
        send('emailSignup', email, username, link, callback);
    }

    function forgotPassword(email, username, token, callback) {
        var link = '<a href="' + config.url + '/forgot-password/' + token + '">' + config.emailForgotPassword.linkText + '</a>';
        send('emailForgotPassword', email, username, link, callback);
    }

    return {
        signup: signup,
        forgotPassword: forgotPassword
    };
};
