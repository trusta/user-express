'use strict';

var crypto = require('crypto');
require('date-utils');

module.exports = function (logger) {

    function getUserId(token) {
        if (token) {
            return token.substr(0, 24);
        } else {
            return null;
        }
    }

    function getToken(str, userId) {
        var token = crypto.createHash('sha256').update(str).digest('hex');
        return userId + token;
    }

    function checkToken(token, user, getToken) {
        if ('string' !== typeof token) {
            return '';
        }
        var t = getToken(user);
        if (t === '') {
            return false;
        }
        return token === t;
    }

    function getLoginToken(user) {
        if (!user) {
            logger.info('user not defined');
            return '';
        }
        var str = user.name + user.email + user.signupTimestamp + user.currentLoginTime + user.previousLoginTime;
        return getToken(str, user._id);
    }

    function checkLoginToken(token, user) {
        var ref = new Date();
        ref.removeDays(1);
        return ref.isBefore(user.currentLoginTime) && checkToken(token, user, getLoginToken);
    }

    function getSignUpToken(user) {
        if (!user) {
            logger.info('user not defined');
            return '';
        }
        var str = user.name + user.email + user.signupTimestamp + user.emailVerificationTimestamp + user.emailVerified;
        return getToken(str, user._id);
    }

    function checkSignUpToken(token, user) {
        return checkToken(token, user, getSignUpToken);
    }

    function getForgotPasswordToken(user) {
        if (!user) {
            logger.info('user not defined');
            return '';
        }
        var str = user.name + user.email + user.signupTimestamp + user.forgotPasswordVerification;
        return getToken(str, user._id);
    }

    function checkForgotPasswordToken(token, user) {
        return checkToken(token, user, getForgotPasswordToken);
    }

    return {
        getUserId: getUserId,
        getLoginToken: getLoginToken,
        checkLoginToken: checkLoginToken,
        getSignUpToken: getSignUpToken,
        checkSignUpToken: checkSignUpToken,
        getForgotPasswordToken: getForgotPasswordToken,
        checkForgotPasswordToken: checkForgotPasswordToken
    };
};
