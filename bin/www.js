'use strict';

/**
 * Module dependencies
 */

var express = require('express');
var winston = require('winston');
var logger = new(winston.Logger)({
    levels: {
        trace: 0,
        debug: 1,
        info: 2,
        warn: 3,
        error: 4
    },
    colors: {
        trace: 'magenta',
        debug: 'blue',
        info: 'green',
        warn: 'yellow',
        error: 'red'
    }
});
logger.add(winston.transports.Console, {
    level: 'debug',
    prettyPrint: true,
    colorize: 'all',
    silent: false,
    timestamp: true
});

var config = require('./config');
var userExpress = require('../app')(config, logger);

var app = express();

app.set('port', process.env.PORT || 9000);
app.use(userExpress.router);


// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/**
 * Start Server
 */

app.listen(app.get('port'), function() {
    logger.info('Express server listening on port ' + app.get('port'));
});
