'use strict';

var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var ObjectID = mongodb.ObjectID;

module.exports = function (config, logger) {

    // Connection URL
    var url = config.db.url + config.db.name;
    // Use connect method to connect to the Server
    function connect(callback) {
        logger.debug('Connect to database');
        MongoClient.connect(url, function (err, db) {
            if (err) {
                callback(null);
            } else {
                callback(db);
            }
        });
    }

    function createUser(user, callback) {
        connect(function (db) {
            if (!db) {
                callback(true);
            } else {
                logger.debug('Create User :', user);
                var collection = db.collection('users');
                collection.insert(user, function (err, res) {
                    db.close();
                    if (err) {
                        callback(err);
                    } else {
                        var user = res.ops[0];
                        user._id = user._id.toString();
                        logger.debug('User created :', user);
                        callback(false, user);
                    }
                });
            }
        });
    }

    function deleteUser(userId, callback) {
        connect(function (db) {
            if (!db) {
                callback(true);
            } else {
                logger.debug('Delete User By ID :', userId);
                var collection = db.collection('users');
                var filter = {
                    _id: ObjectID.createFromHexString(userId)
                };
                collection.deleteOne(filter, function (err) {
                    db.close();
                    if (err) {
                        callback(err);
                    } else {
                        callback(false);
                    }
                });
            }
        });
    }

    function findUserById(userId, callback) {
        connect(function (db) {
            if (!db) {
                callback(true);
            } else {
                logger.debug('Find User By ID :', userId);
                var collection = db.collection('users');
                var filter = {
                    _id: ObjectID.createFromHexString(userId)
                };
                collection.findOne(filter, function (err, user) {
                    db.close();
                    if (err) {
                        callback(err);
                    } else {
                        if (user && user._id) {
                            user._id = user._id.toString();
                        }
                        callback(false, user);
                    }
                });
            }
        });
    }

    function findUserByName(name, callback) {
        connect(function (db) {
            if (!db) {
                callback(true);
            } else {
                logger.debug('Find User By name :', name);
                var collection = db.collection('users');
                var filter = {
                    name: name
                };
                collection.findOne(filter, function (err, user) {
                    db.close();
                    if (err) {
                        callback(err);
                    } else {
                        if (user && user._id) {
                            user._id = user._id.toString();
                        }
                        callback(false, user);
                    }
                });
            }
        });
    }

    function findUserByEmail(email, callback) {
        connect(function (db) {
            if (!db) {
                callback(true);
            } else {
                logger.debug('Find User By Email :', email);
                var collection = db.collection('users');
                var filter = {
                    email: email
                };
                collection.findOne(filter, function (err, user) {
                    db.close();
                    if (err) {
                        callback(err);
                    } else {
                        if (user && user._id) {
                            user._id = user._id.toString();
                        }
                        callback(false, user);
                    }
                });
            }
        });
    }

    function findOtherUserByName(userId, name, callback) {
        connect(function (db) {
            if (!db) {
                callback(true);
            } else {
                logger.debug('Find User By name :', name);
                var collection = db.collection('users');
                var filter = {
                    _id: {$ne: ObjectID.createFromHexString(userId)},
                    name: name
                };
                collection.findOne(filter, function (err, user) {
                    db.close();
                    if (err) {
                        callback(err);
                    } else {
                        if (user && user._id) {
                            user._id = user._id.toString();
                        }
                        callback(false, user);
                    }
                });
            }
        });
    }

    function findOtherUserByEmail(userId, email, callback) {
        connect(function (db) {
            if (!db) {
                callback(true);
            } else {
                logger.debug('Find User By Email :', email);
                var collection = db.collection('users');
                var filter = {
                    _id: {$ne: ObjectID.createFromHexString(userId)},
                    email: email
                };
                collection.findOne(filter, function (err, user) {
                    db.close();
                    if (err) {
                        callback(err);
                    } else {
                        if (user && user._id) {
                            user._id = user._id.toString();
                        }
                        callback(false, user);
                    }
                });
            }
        });
    }

    function updateUser(user, callback) {
        connect(function (db) {
            if (!db) {
                callback(true);
            } else {
                logger.debug('Update User :', user);
                var collection = db.collection('users');

                user._id = ObjectID.createFromHexString(user._id);
                var filter = {
                    _id: user._id
                };
                var update = {
                    $set: user
                };
                collection.updateOne(filter, update, function (err) {
                    db.close();
                    if (err) {
                        callback(err);
                    } else {
                        callback(false);
                    }
                });
            }
        });
    }

    function updateUserAndRemoveAttributes(user, attributesToremove, callback) {
        connect(function (db) {
            if (!db) {
                callback(true);
            } else {
                logger.debug('Update User :', user);
                var collection = db.collection('users');

                user._id = ObjectID.createFromHexString(user._id);
                var filter = {
                    _id: user._id
                };
                var update = {
                    $set: user,
                    $unset: attributesToremove
                };
                collection.updateOne(filter, update, function (err) {
                    db.close();
                    if (err) {
                        callback(err);
                    } else {
                        callback(false);
                    }
                });
            }
        });
    }

    return {
        createUser: createUser,
        deleteUser: deleteUser,
        findUserById: findUserById,
        findUserByName: findUserByName,
        findUserByEmail: findUserByEmail,
        updateUser: updateUser,
        updateUserAndRemoveAttributes: updateUserAndRemoveAttributes,
        findOtherUserByEmail: findOtherUserByEmail,
        findOtherUserByName: findOtherUserByName
    };

};
