'use strict';

/**
 * Module dependencies
 */
var express = require('express');
var mailService = require('../services/mailService');
var tokenServiceFunc = require('../services/tokenService');
var validator = require('validator');
var responseBuilder = require('../services/responseBuilder');
var userServiceFunc = require('../services/userService');

module.exports = function (config, logger) {
    var app = express.Router();
    var mailer = mailService(config, logger);
    var tokenService = tokenServiceFunc(logger);
    var userService = userServiceFunc(config, logger);

    // Register a user
    app.post('/register', function (req, res) {

        // Init new user
        logger.debug('register body :', req.body);
        var name = req.body.name;
        var email = req.body.email;
        var password = req.body.password;

        var error = null;
        // Check data
        if (!name || !email || !password) {
            error = 'All fields are required (name,email,password)';
        } else if (!validator.isEmail(email)) {
            error = 'Email is invalid';
        }

        if (error) {
            logger.info(error);
            return responseBuilder.error(res, error);
        }

        userService.register(name, email, password, function (error, user) {
            if (error) {
                logger.error(error);
                return responseBuilder.error(res, error);
            }

            var token = tokenService.getSignUpToken(user);
            if (!token) {
                logger.error('Error cannot getSignUpToken for user with ID', user._id);
                return responseBuilder.error(res);
            }

            if (!user.emailVerificationTimestamp) {
                return responseBuilder.success(res);
            }

            mailer.signup(user.email, user.name, token, function (error) {
                if (error) {
                    logger.error(error);
                    return responseBuilder.error(res);
                }
                return responseBuilder.success(res);
            });
        });
    });

    // Valid a user
    app.get('/register/:token', function (req, res) {
        var token = req.params.token;
        logger.debug('tokenStr in the route :', token);

        var userId = tokenService.getUserId(token);

        userService.findUserById(userId, function (err, user) {
            if (err) {
                logger.error(err);
                return responseBuilder.error(res);
            }

            if (!tokenService.checkSignUpToken(token, user)) {
                return responseBuilder.error(res, 'Invald token');
            }
            userService.verifyEmail(user, function (err) {
                if (err) {
                    logger.error(err);
                    return responseBuilder.error(res, err);
                }
                return responseBuilder.success(res);
            });
        });
    });


    // Resend confirmation email
    app.post('/register/resend', function (req, res) {

        // Init new user
        logger.debug('register/resend body :', req.body);
        var email = req.body.email;

        var error = null;
        // Check data
        if (!email) {
            error = 'All fields are required (email)';
        } else if (!validator.isEmail(email)) {
            error = 'Email is invalid';
        }

        if (error) {
            logger.info(error);
            return responseBuilder.error(res, error);
        }

        userService.resendConfirmationEmail(email, function (err, user) {
            if (err) {
                logger.error(err);
                return responseBuilder.error(res, err);
            }

            var token = tokenService.getSignUpToken(user);
            if (!token) {
                logger.error('Error cannot getSignUpToken for user with ID', user._id);
                return responseBuilder.error(res);
            }

            mailer.signup(user.email, user.name, token, function (error) {
                if (error) {
                    logger.error(error);
                    return responseBuilder.error(res);
                }
                return responseBuilder.success(res);
            });
        });
    });
    return app;
};
