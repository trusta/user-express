'use strict';

/**
 * Module dependencies
 */
var express = require('express');
var responseBuilder = require('../services/responseBuilder');
var userServiceFunc = require('../services/userService');
var tokenServiceFunc = require('../services/tokenService');
var mailService = require('../services/mailService');

module.exports = function (config, logger) {
    var app = express.Router();

    var mailer = mailService(config, logger);
    var tokenService = tokenServiceFunc(logger);
    var userService = userServiceFunc(config, logger);

    logger.info('Defining forgotPasswordCtrl');
    //     POST /user/forgot-password
    // {
    //  pseudo
    //  (captcha)
    // }
    // -> Désactive le compte
    // -> Génère Token (dans le mail) : ID+'|'+Hash(Pseudo,email,signupDate,forgotPasswordSendDate)
    // -> Retourne sur quel email il a été envoyé (en mode caché)
    app.post('/forgot-password', function (req, res) {
        logger.debug('forgot-password body :', req.body);

        var email = req.body.email;

        if (!email) {
            return responseBuilder.error('All fields are required (email)');
        }

        userService.forgotPassword(email, function (err, user) {
            if (err) {
                return responseBuilder.error(res, err);
            }

            var token = tokenService.getForgotPasswordToken(user);

            mailer.forgotPassword(user.email, user.name, token, function (error) {
                if (error) {
                    logger.error(error);
                    return responseBuilder.error(res);
                }
                return responseBuilder.success(res);
            });
        });

    });


    // POST /user/forgot-password/:token
    // {
    //  password
    //  passwordConfirm
    // }
    // -> Vérifie les deux password
    // -> Vérifie le token
    // -> Génère salt
    // -> Update User (password et forgotPasswordSendDate [à null])
    app.post('/forgot-password/:token', function (req, res) {
        logger.debug('forgot-password body :', req.body);

        var password = req.body.password;
        var token = req.params.token;

        if (!password) {
            return responseBuilder.error('All fields are required (password)');
        }

        var userId = tokenService.getUserId(token);
        if (!userId) {
            logger.info('Invalid token, no userId');
            return responseBuilder.error(res, 'Invalid token');
        }

        userService.findUserById(userId, function (err, user) {
            if (err) {
                return responseBuilder.error(res, err);
            }

            if (!tokenService.checkForgotPasswordToken(token, user)) {
                logger.info('Invalid token');
                return responseBuilder.error(res, 'Invalid token');
            }

            userService.resetPassword(user, password, function (err) {
                if (err) {
                    return responseBuilder.error(res, err);
                }
                return responseBuilder.success(res);
            });
        });

    });

    return app;
};
