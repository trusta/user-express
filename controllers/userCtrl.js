'use strict';

/**
 * Module dependencies
 */
var express = require('express');
var validator = require('validator');
var userServiceFunc = require('../services/userService');
var responseBuilder = require('../services/responseBuilder');
var tokenServiceFunc = require('../services/tokenService');
var restrictFunc = require('../middlewares/restrict');

module.exports = function (config, logger) {

    var app = express.Router();

    var userService = userServiceFunc(config, logger);
    var restrict = restrictFunc(config, logger);
    var tokenService = tokenServiceFunc(logger);

    logger.info('Defining userCtrl');
    // DEL /user
    // -> avec le token
    // -> supprime le user
    app.delete('/user', restrict.user, function (req, res) {
        logger.debug('del user');

        var userId = tokenService.getUserId(req.session.token);

        if (!userId) {
            return responseBuilder.error(res);
        }

        userService.deleteUser(userId, function (err) {
            if (err) {
                return responseBuilder.error(res);
            }

            logger.debug('Token set to null');
            req.session.token = null;
            return responseBuilder.success(res);
        });

    });


    // PUT /user
    // {
    //     email
    //     name
    //     password
    // }
    // -> Avec le token
    // -> Vérifie le caratère Unique (par exeption mongodb)
    // -> Update le user
    // TODO Vérifier l'email unique et le username unique
    app.put('/user', restrict.user, function (req, res) {
        logger.debug('update user body :', req.body);

        var userObj = {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        };

        if (!userObj.name || !userObj.email || !userObj.password) {
            return responseBuilder.error(res, 'All fields are required (name, email, password)');
        }

        if (!validator.isEmail(userObj.email)) {
            return responseBuilder.error(res, 'Invalid email');
        }

        var userId = tokenService.getUserId(req.session.token);

        if (!userId) {
            logger.error('User not logged in try to update it, user :', userObj);
            return responseBuilder.error(res);
        }


        userService.updateUser(userId, userObj, function (err, user) {
            if (err) {
                return responseBuilder.error(res, err);
            }
            var userReturned = {
                name: userObj.name,
                email: userObj.email
            };
            req.session.token = tokenService.getLoginToken(user);
            return responseBuilder.success(res, userReturned);
        });


    });


    // PUT /user/password
    // {
    //     oldPassword
    //     newPassword
    //     newPasswordConfirm
    // }
    // -> Avec le token
    // -> Vérifie le oldPassword
    // -> Vérifie new et newConfirm
    // -> Génère new salt
    // -> Update user
    app.put('/user/password', restrict.user, function (req, res) {
        logger.debug('update user password body :', req.body);

        var password = req.body.password;
        var newPassword = req.body.newPassword;

        if (!password || !newPassword) {
            return responseBuilder.error(res, 'All fields are required (password, newPassword)');
        }

        var userId = tokenService.getUserId(req.session.token);

        if (!userId) {
            return responseBuilder.error(res);
        }


        userService.changePassword(userId, password, newPassword, function (err) {
            if (err) {
                return responseBuilder.error(res, err);
            }

            return responseBuilder.success(res);
        });


    });

    // GET /user
    // -> Avec token
    // -> Retourne email et pseudo
    app.get('/user', restrict.user, function (req, res) {
        logger.debug('in get user');

        var userId = tokenService.getUserId(req.session.token);

        if (!userId) {
            return responseBuilder.error(res);
        }

        userService.findUserById(userId, function (err, user) {
            if (err) {
                return responseBuilder.error(res, err);
            }

            var returnedUser = {
                name: user.name,
                email: user.email
            };

            return responseBuilder.success(res, returnedUser);
        });
    });
    return app;
};
