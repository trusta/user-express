'use strict';

/**
 * Module dependencies
 */
var express = require('express');
var responseBuilder = require('../services/responseBuilder');
var userServiceFunc = require('../services/userService');
var tokenServiceFunc = require('../services/tokenService');
var validator = require('validator');

module.exports = function (config, logger) {
    var app = express.Router();
    var tokenService = tokenServiceFunc(logger);
    var userService = userServiceFunc(config, logger);

    logger.info('Defining loginCtrl');

    app.post('/login', function (req, res) {

        logger.debug('login body :', req.body);
        // Email or name
        var login = req.body.login;
        var password = req.body.password;

        // Check data
        if (!login || !password) {
            var error = 'All fields are required (login,password)';
            logger.info(error);
            return responseBuilder.error(res, error);
        }

        var loginObj = {
            password: password
        };

        if (validator.isEmail(login)) {
            loginObj.email = login;
        } else {
            loginObj.name = login;
        }

        userService.login(loginObj, function (err, user) {
            if (err) {
                logger.error(err);
                return responseBuilder.error(res, err);
            }

            logger.info('user logged in :', user);

            req.session.token = tokenService.getLoginToken(user);
            return responseBuilder.success(res);
        });
    });
    // -> Retourne le Token dans un cookie
    // -> Gère un nombre maximum de tentatives ?

    // GET /user/logout
    // -> avec le token
    // -> Set le previousLoginDate à currentLoginDate
    // -> Set le currentLoginDate à null (invalide le token)
    app.get('/logout', function (req, res) {
        var token = req.session.token;
        if (!token) {
            logger.debug('Token null');
            return responseBuilder.success(res);
        }
        logger.debug('token :', token);

        var userId = tokenService.getUserId(token);

        userService.findUserById(userId, function (err, user) {
            if (err) {
                return responseBuilder.error(res);
            } else if (!tokenService.checkLoginToken(token, user)) {
                logger.debug('Token invalid');
                return responseBuilder.success(res);
            }
            userService.logout(user, function (err) {
                if (err) {
                    return responseBuilder.error(res);
                }

                logger.debug('Token set to null');
                req.session.token = null;
                return responseBuilder.success(res);
            });
        });
    });
    return app;
};
