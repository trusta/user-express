'use strict';

/**
 * Module dependencies
 */
var express = require('express');
var forgotPasswordCtrl = require('./controllers/forgotPasswordCtrl');
var loginCtrl = require('./controllers/loginCtrl');
var registerCtrl = require('./controllers/registerCtrl');
var userCtrl = require('./controllers/userCtrl');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var restrictFunc = require('./middlewares/restrict');

module.exports = function (config, logger) {

    // router
    var router = express.Router();

    router.use(bodyParser.json());
    router.use(bodyParser.urlencoded({
        extended: true
    }));
    router.use(cookieParser());
    router.use(cookieSession({
        secret: config.cookieSecret
    }));

    // add submodule routes
    router.use(forgotPasswordCtrl(config, logger));
    router.use(loginCtrl(config, logger));
    router.use(registerCtrl(config, logger));
    router.use(userCtrl(config, logger));

    var utils = {
        restrict: restrictFunc(config, logger)
    };

    return {
        router: router,
        utils: utils
    };
};
